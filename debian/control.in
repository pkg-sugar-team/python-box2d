Source: python-box2d
Section: python
Priority: optional
Maintainer: Debian Sugar Team <pkg-sugar-devel@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Build-Depends: @cdbs@
Standards-Version: 4.1.0
Vcs-Git: https://salsa.debian.org/pkg-sugar-team/python-box2d.git
Vcs-Browser: https://salsa.debian.org/pkg-sugar-team/python-box2d
Homepage: https://github.com/pybox2d/pybox2d

Package: python-box2d
Architecture: any
Depends: ${python:Depends}, ${misc:Depends}, ${shlibs:Depends}
Provides: ${python:Provides}
Description: 2D Game Physics for Python
 pybox2d is a 2D physics library for your games and simple simulations.
 It's based on the Box2D library, written in C++. It supports several
 shape types (circle, polygon, thin line segments), and quite a few
 joint types (revolute, prismatic, wheel, etc.).
 .
 This package contains the Python library.

Package: python-box2d-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ttf-bitstream-vera
Suggests: python-box2d, python-pygame
Description: 2D Game Physics for Python - documentation
 pybox2d is a 2D physics library for your games and simple simulations.
 It's based on the Box2D library, written in C++. It supports several
 shape types (circle, polygon, thin line segments), and quite a few
 joint types (revolute, prismatic, wheel, etc.).
 .
 This package contains documentation.
